<?php
$count = 10;
$arr = range(0, 500);
shuffle($arr);
$arr = array_slice($arr, 0, $count);
$maxKey = array_search(max($arr), $arr);
$minKey = array_search(min($arr), $arr);

print '<pre>';
print_r($arr);
print '</pre>';

print '<p>минимальное число в массиве - ' . min($arr) . '</p>';
print '<p>максимальное число в массиве - ' . max($arr) . '</p>';

list($arr[$minKey], $arr[$maxKey]) = array($arr[$maxKey], $arr[$minKey]);

print 'массив после изменения';
print '<pre>';
print_r($arr);
print '</pre>';