<form method="POST" action="5.php">
    <label for="name">Введите ФИО</label>
    <input id="name" name="name" type="text">
    <button type="submit">Отправить</button>
</form>
<style>
    label {
        display: block;
    }
</style>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['name'];
    $arr = explode(' ', $name);

    foreach($arr as $key => $value) {
        if($key != 0) {
            $value = ' ' . substr($value, 0, 1) . '. ';
        }

        $newArr[] = $value;
    }

    $newName = implode($newArr);

    print $newName;
}
