<form method="POST" action="8.php">
    <label for="number">Введите номер карты от 6 до 14</label>
    <input id="number" name="number" type="text">
    <button type="submit">Отправить</button>
</form>
<style>
    label {
        display: block;
    }
</style>
<?php
function getValue($number){
    $arr = [
        '6' => 'шестёрка',
        '7' => 'семёрка',
        '8' => 'восьмёрка',
        '9' => 'девятка',
        '10' => 'десятка',
        '11' => 'валет',
        '12' => 'дама',
        '13' => 'король',
        '14' => 'туз'
    ];
    return $arr[$number];
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $number = intval($_POST['number']);
    if ($number >= 6 && $number <= 14) {
        print 'ваша карта - ' . getValue($number);
    }
}