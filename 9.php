<form method="POST" action="9.php">
    <label for="year">Введите год начиная с 1924</label>
    <input id="year" name="year" type="text">
    <button type="submit">Отправить</button>
</form>
<style>
    label {
        display: block;
    }
</style>

<?php
function getChinaYear($year){
    $arr = ['Крыса', 'Бык', 'Тигр', 'Кролик', 'Дракон', 'Змея', 'Лошадь', 'Коза', 'Обезьяна', 'Петух', 'Собака', 'Свинья'];
    $startYear = 1924;
    $arrKey = ($year - $startYear) % 12;
    if ($startYear > $year) {
        return false;
    }

    return $arr[$arrKey];
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $year = intval($_POST['year']);
    print getChinaYear($year);
}