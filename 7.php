<form method="POST" action="7.php">
    <label for="year">Введите год от 1 до 9999</label>
    <input id="year" name="year" type="text">
    <button type="submit">Отправить</button>
</form>
<style>
    label {
        display: block;
    }
</style>
<?php
function getSleapYear($year) {
    if($year <= 0 || $year > 9999){
        return 'Введите год от 1 до 9999';
    }elseif ($year % 400 == 0 || $year % 4 == 0) {
        return 'високосный год';
    } else {
        return 'невисокосный год';
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $year = intval($_POST['year']);
    print getSleapYear($year);
}