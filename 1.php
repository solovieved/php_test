<form method="POST" action="1.php">
    <label for="number">Ведите число сумму цифр которого вы хотите узнать</label>
    <input id="number" name="number" type="text">
    <button type="submit">Отправить</button>
</form>
<style>
    label {
        display: block;
    }
</style>

<?php
function getSum($number){
    $arr = str_split($number); 
    $sum = 0;
    foreach ($arr as $val) {
        $sum += intval($val); 
    }
    return $sum;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = $_POST['number'];
    print 'сумма цифр - ' . getSum($data);
}