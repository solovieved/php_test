<form method="POST" action="2.php">
    <label for="number">Ведите число в котором вы хотите узнать количество вхождений</label>
    <input id="number" name="number" type="text">
    <label for="countnumber">Введите цифру количество повторений которой вы хотите узнать</label>
    <input id="countnumber" name="countnumber" type="text">
    <button type="submit">Отправить</button>
</form>
<style>
    label, button {
        display: block;
    }
</style>
<?php
function getCount($number, $countNum){
    $arr = str_split($number);
    $count = array_count_values($arr);
    if (isset($count[$countNum])) {
        return 'цифра ' . $countNum . ' повторяется ' . $count[$countNum] . ' раз(а)';
    }
    return 'такой цифры нет';
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $number = $_POST['number'];
    $countNum = $_POST['countnumber'];
    print getCount($number, $countNum);
}