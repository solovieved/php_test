<form method="POST" action="6.php">
    <label for="minute">Введите число(количество минут прошедших сначала часа), что-бы узнать какой светофор сейчас горит</label>
    <input id="minute" name="minute" type="text">
    <button type="submit">Отправить</button>
</form>
<style>
    label {
        display: block;
    }
</style>
<?php

function getColor($minute) {
    if($minute >= 60) {
        return 'в часу только 60 мин';
    }elseif($minute % 5 > 0 && $minute % 5 <= 3) {
        return 'горит зелёный';
    } else {
        return 'горит красный';
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $minute = $_POST['minute'];
    print getColor($minute);
}